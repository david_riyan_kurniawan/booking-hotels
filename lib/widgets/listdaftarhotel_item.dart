import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';

class listdaftarhotelItem extends StatelessWidget {
  const listdaftarhotelItem({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 15.0),
      child: Column(
        children: [
          Row(
            children: [
              Container(
                height: 110,
                width: 110,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    image: DecorationImage(
                        image: AssetImage('assets/images/hotel-1.png'),
                        fit: BoxFit.cover)),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 10.0),
                child: Column(
                  children: [
                    Text(
                      'Shangrilla Hotel',
                      style: GoogleFonts.urbanist(
                          fontWeight: FontWeight.w800, fontSize: 16),
                    ),
                    Text(
                      '211B Baker Street',
                      style: GoogleFonts.urbanist(
                          fontWeight: FontWeight.w500,
                          fontSize: 16,
                          color: Colors.black54),
                    )
                  ],
                ),
              ),
              Spacer(),
              Column(
                children: [
                  Text(
                    'Rp.300.000',
                    style: GoogleFonts.urbanist(
                        fontWeight: FontWeight.w800,
                        fontSize: 16,
                        color: Colors.black),
                  ),
                  Row(
                    children: [
                      SizedBox(
                          height: 16,
                          width: 16,
                          child: SvgPicture.asset(
                            'assets/svgs/stars.svg',
                            fit: BoxFit.cover,
                          )),
                      Text(
                        '4.8',
                        style: GoogleFonts.urbanist(
                            fontWeight: FontWeight.w500,
                            fontSize: 16,
                            color: Colors.black54),
                      )
                    ],
                  )
                ],
              )
            ],
          ),
          SizedBox(
            height: 13,
          ),
          Row(
            children: [
              Container(
                height: 110,
                width: 110,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    image: DecorationImage(
                        image: AssetImage('assets/images/hotel-1.png'),
                        fit: BoxFit.cover)),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 10.0),
                child: Column(
                  children: [
                    Text(
                      'Shangrilla Hotel',
                      style: GoogleFonts.urbanist(
                          fontWeight: FontWeight.w800, fontSize: 16),
                    ),
                    Text(
                      '211B Baker Street',
                      style: GoogleFonts.urbanist(
                          fontWeight: FontWeight.w500,
                          fontSize: 16,
                          color: Colors.black54),
                    )
                  ],
                ),
              ),
              Spacer(),
              Column(
                children: [
                  Text(
                    'Rp.300.000',
                    style: GoogleFonts.urbanist(
                        fontWeight: FontWeight.w800,
                        fontSize: 16,
                        color: Colors.black),
                  ),
                  Row(
                    children: [
                      SizedBox(
                          height: 16,
                          width: 16,
                          child: SvgPicture.asset(
                            'assets/svgs/stars.svg',
                            fit: BoxFit.cover,
                          )),
                      Text(
                        '4.8',
                        style: GoogleFonts.urbanist(
                            fontWeight: FontWeight.w500,
                            fontSize: 16,
                            color: Colors.black54),
                      )
                    ],
                  )
                ],
              )
            ],
          ),
          SizedBox(
            height: 13,
          ),
          Row(
            children: [
              Container(
                height: 110,
                width: 110,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    image: DecorationImage(
                        image: AssetImage('assets/images/hotel-1.png'),
                        fit: BoxFit.cover)),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 10.0),
                child: Column(
                  children: [
                    Text(
                      'Shangrilla Hotel',
                      style: GoogleFonts.urbanist(
                          fontWeight: FontWeight.w800, fontSize: 16),
                    ),
                    Text(
                      '211B Baker Street',
                      style: GoogleFonts.urbanist(
                          fontWeight: FontWeight.w500,
                          fontSize: 16,
                          color: Colors.black54),
                    )
                  ],
                ),
              ),
              Spacer(),
              Column(
                children: [
                  Text(
                    'Rp.300.000',
                    style: GoogleFonts.urbanist(
                        fontWeight: FontWeight.w800,
                        fontSize: 16,
                        color: Colors.black),
                  ),
                  Row(
                    children: [
                      SizedBox(
                          height: 16,
                          width: 16,
                          child: SvgPicture.asset(
                            'assets/svgs/stars.svg',
                            fit: BoxFit.cover,
                          )),
                      Text(
                        '4.8',
                        style: GoogleFonts.urbanist(
                            fontWeight: FontWeight.w500,
                            fontSize: 16,
                            color: Colors.black54),
                      )
                    ],
                  )
                ],
              )
            ],
          ),
        ],
      ),
    );
  }
}
